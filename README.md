This is a tictactoe/gomoku game written in pure JS.
It is playable in the browser for two local players.
The length of the winning state (how many same colored buttons are next to/under each other) is selectable by the player(s). The options are from 3 to 6 length.
There are several different sized maps, starting from 10x10 to 16x16.
When a player wins, the winning buttons' color turn to gold, and an alert box appears that shows the name of the winner.
When a game ends, after 2 secs another one starts.